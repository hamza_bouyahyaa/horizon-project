import { combineReducers } from "@reduxjs/toolkit";
import { reducer as userReducer } from "./../slices/user";
import { reducer as modalsReducer } from "./../slices/modals";
import { reducer as notifierReducer } from "../slices/notifier";

const combinedReducer = combineReducers({
  user: userReducer,
  notifier: notifierReducer,
  modals: modalsReducer,
});
const rootReducer = (state, action) => {
  if (action.type === "LOGOUT") {
    state = undefined;
  }
  return combinedReducer(state, action);
};
export default rootReducer;
