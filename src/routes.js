import React, { Suspense, Fragment, lazy } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { AuthGuard, GuestGuard, Loader, RoleGuard } from "./components";

import MainLayout from "./layouts/MainLayout";

export const renderRoutes = (routes = []) => {
  return (
    <Suspense fallback={<Loader />}>
      <Switch>
        {routes.map((route, i) => {
          const Guard = route.guard || Fragment;
          const Layout = route.layout || Fragment;
          const Component = route.component;

          return (
            <Route
              key={i}
              path={route.path}
              exact={route.exact}
              render={(props) => (
                <Guard>
                  <Layout>
                    {route.routes ? (
                      renderRoutes(route.routes)
                    ) : (
                      <Component {...props} />
                    )}
                  </Layout>
                </Guard>
              )}
            />
          );
        })}
      </Switch>
    </Suspense>
  );
};
export const routes = [
  {
    exact: true,
    path: "/login",
    guard: GuestGuard,
    component: lazy(() => import("./pages/Auth/Login/Login")),
  },
  {
    exact: true,
    path: "/sign-up",
    guard: GuestGuard,
    component: lazy(() => import("./pages/Auth/SignUp/SignUp")),
  },
  {
    exact: true,
    path: "/",
    guard: AuthGuard,
     component: lazy(() => import("./pages/Main")),
  },

];

export default routes;
