export const isFound = (array, id) => {
  let isFound = false;
  array.forEach((item) => (item._id === id ? (isFound = true) : null));

  return isFound;
};
