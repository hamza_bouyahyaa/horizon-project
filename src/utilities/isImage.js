export const isImage = (url) => {
  return /\.(jpg|jpeg|png|webp|avif|gif|svg|webp)$/.test(url);
};
