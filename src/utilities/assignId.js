export const assignedId = (conversations, message) => {
  let id;
  conversations.forEach(
    (conversation) =>
      conversation._id === message.conversation &&
      (id = conversation.assigned.user._id)
  );
  return id;
};
