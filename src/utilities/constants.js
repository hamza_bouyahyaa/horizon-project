import { nanoid } from "nanoid";
import { faker } from "@faker-js/faker";
import { sample } from "lodash";
import { dashToEmptySpace } from "./dashToEmptySpace";
import image1 from "./../assets/img/image1.png"
import image2 from "./../assets/img/image2.png"
import image3 from "./../assets/img/image3.png"
import image5 from "./../assets/img/image5.png"
import image6 from "./../assets/img/image6.png"
import {
  analyticsIcon,
  commentBlockIcon,
  helpDeskIcon,
  logOutIcon,
  messagesIcon,
  file,
  location,
  eye,
  categories,
  vector,
  fileedit,
  openusers,
  calendar,
  commentquestion,
  close,
  edit,
  plusCircleIcon,
  settingsIcon,
  todoListIcon,
  userCircleIcon,
  account,
  helpdesk,
  notification,
  inbox,
  assigned,
  pack,
  users,
  shortcutmessages,
  chakkebAlBacIcon,
  mloulLelekherIcon,
  silverIcon,
  ipIcon,
  googleIcon,
  editIcon,
  envelopeIcon,
  clipboardIcon,
  trashIcon,
  sidaInfoServiceIcon,
} from "../assets";
export default {
  APPBAR_MOBILE: 64,
  APPBAR_DESKTOP: 40,
  DRAWER_WIDTH: 239,
  APP_BAR_MOBILE: 20,
  APP_BAR_DESKTOP: 50,
  HEADER_DIVIDER_WIDTH: 32.6,
  USERLIST: [...Array(10)].map((_, index) => ({
    id: faker.datatype.uuid(),
    avatarUrl: faker.internet.avatar(),
    name: faker.name.fullName(),
    email: faker.internet.email(),
    role: sample([
      "@Sales-Team",
      "@Sales-Team",
      "@Sales-Team",
      "@Sales-Team",
      "@Sales-Team",
      "@Sales-Team",
      "@Sales-Team",
      "@Sales-Team",
      "@Sales-Team",
      "@Sales-Team",
    ]),
  })),
  OPTIONS: [
    // { value: "ADMIN", label: "admin" },
    { value: "OPERATOR", label: "operator" },
  ],

  MANAGE_SHORTCUTS: [
    { id: nanoid(), title: "Service Vente" },
    { id: nanoid(), title: "Customer Service" },
  ],
  SHORTCUT_OPTIONS: [
    { value: "chocolate", label: "Chocolate" },
    { value: "strawberry", label: "Strawberry" },
    { value: "vanilla", label: "Vanilla" },
  ],

  NAV_CONFIG: [
    {
      id: nanoid(),
      title: "Messaging",
      path: "/messaging",
      icon: messagesIcon,
    },
    {
      id: nanoid(),
      title: "To Do List",
      path: "/todo-list",
      icon: todoListIcon,
      children: [
        {
          id: nanoid(),
          title: "Open",
          path: "/todo-list/Open",
          icon: inbox,
        },
        {
          id: nanoid(),
          title: "Assigned",
          path: "/todo-list/assigned",
          icon: assigned,
        },
        {
          id: nanoid(),
          title: "Complete",
          path: "/todo-list/complete",
          icon: pack,
        },
      ],
    },
    {
      id: nanoid(),
      title: "Helpdesk",
      path: "/help-desk",
      icon: helpDeskIcon,
      children: [
        {
          id: nanoid(),
          title: "Articles",
          path: "/help-desk/articles",
          icon: file,
        },
        // {
        //   id: nanoid(),
        //   title: "Categories",
        //   path: "/help-desk/categories",
        //   icon: categories,
        // },
        // {
        //   id: nanoid(),
        //   title: "Published",
        //   path: "/help-desk/published",
        //   icon: location,
        // },
        // {
        //   id: nanoid(),
        //   title: "Draft",
        //   path: "/help-desk/draft",
        //   icon: fileedit,
        // },
        {
          id: nanoid(),
          title: "Visible",
          path: "/help-desk/visible",
          icon: eye,
        },
        // {
        //   id: nanoid(),
        //   title: "Hidden",
        //   path: "/help-desk/hidden",
        //   icon: eye,
        // },
      ],
    },
    {
      id: nanoid(),
      title: "Analytics",
      path: "/analytics",
      icon: analyticsIcon,
    },
    {
      id: nanoid(),
      title: "Settings",
      path: "/settings",
      icon: settingsIcon,
      children: [
        {
          id: nanoid(),
          title: "Account",
          subtitle: "Profile,Name,Email,Password",
          path: "/settings/account",
          icon: account,
        },
        // {
        //   id: nanoid(),
        //   title: "Notification",
        //   subtitle: "Manage Your Notification",
        //   path: "/settings/notification",
        //   icon: notification,
        // },
        {
          id: nanoid(),
          title: "Users",
          subtitle: "Manage Your Team",
          path: "/settings/users",
          role: ["ADMIN", "SUPER-ADMIN"],
          icon: users,
        },
        {
          id: nanoid(),
          title: "Shortcut Message",
          subtitle: "Manage Your Saved Shortcut",
          path: "/settings/shortcut-message",
          icon: shortcutmessages,
        },
        {
          id: nanoid(),
          title: "Helpdesk",
          subtitle: "Manage Your helpdesk",
          path: "/settings/helpdesk",
          icon: helpdesk,
        },
      ],
    },
  ],
  FILTERS: [
    { id: nanoid(), label: "Filter 1" },
    { id: nanoid(), label: "Filter 2" },
    { id: nanoid(), label: "Filter 3" },
    { id: nanoid(), label: "TFilter 4" },
    { id: nanoid(), label: "Filter 5" },
    { id: nanoid(), label: "Filter 6" },
    { id: nanoid(), label: "Filter 7" },
  ],
  HELPDESK_ROUTES: [
    {
      id: nanoid(),
      title: "Articles",
      path: "/help-desk/articles",
      icon: file,
    },
    // {
    //   id: nanoid(),
    //   title: "Categories",
    //   path: "/help-desk/categories",
    //   icon: categories,
    // },
    // {
    //   id: nanoid(),
    //   title: "Published",
    //   path: "/help-desk/published",
    //   icon: location,
    // },

    {
      id: nanoid(),
      title: "Visible",
      path: "/help-desk/visible",
      icon: fileedit,
    },
    {
      id: nanoid(),
      title: "Draft",
      path: "/help-desk/draft",
      icon: fileedit,
    },
    // {
    //   id: nanoid(),
    //   title: "Hidden",
    //   path: "/help-desk/hidden",
    //   icon: fileedit,
    // },
  ],
  TODOLIST_ROUTES: [
    {
      id: nanoid(),
      title: "Open",
      path: "/todo-list/open",
      icon: inbox,
    },
    {
      id: nanoid(),
      title: "Assigned",
      path: "/todo-list/assigned",
      icon: assigned,
    },
    {
      id: nanoid(),
      title: "Complete",
      path: "/todo-list/complete",
      icon: pack,
    },
  ],

  accountPopoverConfig: [
    {
      id: nanoid(),
      icon: userCircleIcon,
      alt: "user-circle",
      title: "Edit Account",
      onClick: "/settings/account",
    },
    {
      id: nanoid(),
      icon: plusCircleIcon,
      alt: "plus-circle",
      title: "Invite Your Team",
      onClick: "/",
    },
    {
      id: nanoid(),
      icon: logOutIcon,
      alt: "log-out",
      title: "Logout",
    },
  ],
  HIVINFO: [
    {
      name: "ABOUT HIV",
      image: image1,
      href: 'https://www.cdc.gov/hiv/basics/whatishiv.html'
    },
    {
      name: "HIV PRESENTATION",
      image: image2,
      href: "https://www.mayoclinic.org/diseases-conditions/hiv-aids/symptoms-causes/syc-20373524"
    },
    {
      name: "HIV TESTING",
      image: image3,
      href: "https://www.cdc.gov/hiv/testing/index.html"
    },
    {
      name: "HIV TRANSIMISSION",
      image: image5,
      href: "https://www.cdc.gov/hiv/basics/transmission.html"
    },
    {
      name: "LIVING WITH HIV",
      image: image6,
      href: "https://www.nhs.uk/conditions/hiv-and-aids/living-with/"
    },
    {
      name: "MORE INFO",
      image: sidaInfoServiceIcon,
      href: "https://www.sida-info-service.org"
    },
    
  ]
  ,

  INVITATIONS_LIST: [
    {
      id: "Members",
      title: "Full Members",
    },
    {
      id: "PendingInvites",
      title: "Pending Invites",
    },
    // {
    //   id: "Approval",
    //   title: "Pending For Approval",
    // },
  ],
  ARTICLES_STATUS: [
    {
      value: "DRAFT",
      label: "DRAFT",
    },
    {
      value: "VISIBLE",
      label: "VISIBLE",
    },
    // {
    //   value: "HIDDEN",
    //   label: "HIDDEN",
    // },
  ],
  TABLE_HEAD: [
    { id: "OperatorId", label: "OperatorId", alignRight: false },
    { id: "Full Name", label: "Full Name", alignRight: false },
    { id: "Email", label: "Email", alignRight: false },
    { id: "Role", label: "Role", alignRight: false },
    { id: "phone Number", label: "Phone Number", alignRight: false },
  ],
  PENDING_TABLE_HEAD: [
    { id: "OperatorId", label: "OperatorId", alignRight: false },
    { id: "Full Name", label: "Full Name", alignRight: false },
    { id: "Email", label: "Email", alignRight: false },
    { id: "Role", label: "Role", alignRight: false },
  ],
  SETTINGS: [
    {
      id: nanoid(),
      title: "Account",
      subtitle: "Profile,Name,Email,Password",
      path: "/settings/account",
      icon: account,
    },
    // {
    //   id: nanoid(),
    //   title: "Notification",
    //   subtitle: "Manage Your Notification",
    //   path: "/settings/notification",
    //   icon: notification,
    // },
    {
      id: nanoid(),
      title: "Users",
      subtitle: "Manage Your Team",
      path: "/settings/users",
      icon: users,
    },
    {
      id: nanoid(),
      title: "Shortcut Message",
      subtitle: "Manage Your Saved Shortcut",
      path: "/settings/shortcut-message",
      icon: shortcutmessages,
    },
    {
      id: nanoid(),
      title: "Question",
      subtitle: "Create Bot Question",
      path: "/settings/question",
      icon: helpdesk,
    },
  ],

  FILTER_CARDS_MESSAGES: [
    { id: nanoid(), title: "All" },
    { id: nanoid(), title: "Unread" },
    { id: nanoid(), title: "Unresolved" },
    { id: nanoid(), title: "Resolved" },
  ],

  REPLIES_CONTROLLERS: [
    { id: nanoid(), text: "Reply" },
    { id: nanoid(), text: "Note" },
    { id: nanoid(), text: "Reminder" },
    { id: nanoid(), text: "Shortcuts" },
    { id: nanoid(), text: "Helpdesk" },
  ],
  ISSUES: [
    {
      id: "assignedto",
      title: dashToEmptySpace("Assigned-to:"),
      img: openusers,
      answer: "Empty",
    },
    {
      id: "duedate",
      title: dashToEmptySpace(`Due-Date:`),
      img: calendar,
      answer: "Empty",
    },
    // { id: "tags", title: `Tags:`, img: edit, answer: "Empty" },
  ],

  TAGS: [
    { id: nanoid(), title: "welcome", color: "#ED3863" },
    { id: nanoid(), title: "Chat", color: "#7BC600" },
    { id: nanoid(), title: "Issue", color: "#F3941B" },
    { id: nanoid(), title: "Account Problem", color: "#04C9E4" },
    { id: nanoid(), title: "New Client", color: "#38A1ED" },
    { id: nanoid(), title: "Discount", color: "#6932FA" },
  ],

  ACCOUNT_INFO: {
    Creation: "08:09 PM | 17/05/2022",
    Update: "01:43 PM | 23/05/2022",
  },

  HOVER_MESSAGE_ICONS: [
    { id: nanoid(), icon: editIcon },
    { id: nanoid(), icon: envelopeIcon },
    { id: nanoid(), icon: clipboardIcon },
    { id: nanoid(), icon: trashIcon },
  ],
  OPTIONS_DATE: {},
  FAKE_SKELETONS: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
  ISSUE_DETAILS_LIST: [
    {
      id: "Description",
      title: "Description",
    },
    {
      id: "Comments",
      title: "Comments",
    },
    {
      id: "History",
      title: "History",
    },
  ],
  BACKGROUND_COLORS: [
    "#8A2BE2 ",
    "#52C8FF",
    "#8A2BE2 ",
    "#52C8FF",
    "#8A2BE2 ",
    "#52C8FF",
    "#8A2BE2 ",
    "#52C8FF",
    "#8A2BE2 ",
    "#52C8FF",
    "#8A2BE2 ",
    "#52C8FF",
    "#8A2BE2 ",
    "#52C8FF",
    "#8A2BE2 ",
    "#52C8FF",
    "#8A2BE2 ",
    "#52C8FF",
    "#8A2BE2 ",
    "#52C8FF",
    "#8A2BE2 ",
    "#52C8FF",
  ],
  HOURS_LABELS: [
    [
      "08:00",
      "09:00",
      "10:00",
      "11:00",
      "12:00",
      "13:00",
      "14:00",
      "15:00",
      "16:00",
      "17:00",
      "18:00",
      "19:00",
      "20:00",
      "21:00",
      "22:00",
      "23:00",
      "00:00",
    ],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  ],
  MONTHS_LABEL: [
    [
      "Janvier",
      "Fevrier",
      "Mars",
      "Avril",
      "May",
      "Juin",
      "Juillet",
      "Aout",
      "September",
      "Octobre",
      "Novembre",
      "Décembre",
    ],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  ],
  DAYS_LABEL: [
    ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
    [0, 0, 0, 0, 0, 0, 0],
  ],
};
