import { emitEvent } from "../socket/socket";

export const seenMessage = (messages, messagesStored, user) => {
  let allMessages = [];
  messages?.forEach((item) => {
    item?._id === messagesStored[0]?.conversation &&
      (allMessages = item?.messages);
  });
  let clientMessages = [];
  allMessages.length > 0 &&
    (clientMessages = allMessages?.filter(
      (message) => message.from === "CLIENT"
    ));

  let lastMessageFromClient = {};
  clientMessages.length > 0 &&
    (() => {
      lastMessageFromClient = clientMessages[clientMessages?.length - 1];
      !lastMessageFromClient?.seenBy?.includes(user?._id) &&
        (lastMessageFromClient = {
          ...lastMessageFromClient,
          seenBy: [...lastMessageFromClient.seenBy, user?._id],
        });
    })();

  emitEvent("SET_SEEN", {
    webSiteId: user?.websiteID,
    conversation: messagesStored[0]?.conversation,
    message: lastMessageFromClient,
  });
};
