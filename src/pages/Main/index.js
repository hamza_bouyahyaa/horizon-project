import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import image from '../../assets/img/sida-image.jpg';
import PlayCircleOutlinedIcon from '@mui/icons-material/PlayCircleOutlined';
import { IoCloseOutline } from "react-icons/io5";
import { BiLoaderAlt } from "react-icons/bi";
import "./_index.scss"
import HIV from '../../components/HIV';
import Article from '../../components/Article';
const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        height: '95vh',
        position: 'relative',

        '& img': {
            objectFit: 'cover',
        },
    },
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    title: {
        paddingBottom: theme.spacing(4),
        fontWeight: "bold",
        fontSize: "40px"
    },
}));

const Main = () => {
    const classes = useStyles();
    const [modal, setModal] = useState(false);
    const [videoLoading, setVideoLoading] = useState(true);

    const openModal = () => {
        setModal(!modal);
    };

    const spinner = () => {
        setVideoLoading(!videoLoading);
    };

    return (
        <React.Fragment>
            <section className={classes.root}>
                <img
                    src={image}
                    width="100%"
                    height="100%"
                />
                <div className={classes.overlay}>
                    <Box
                        color="#fff"
                        sx={{ maxWidth: "1000px", margin: "0 auto", paddingTop: "100px" }}
                    >
                        <Typography style={{ margin: "0 auto" }} variant="h3" component="h1" className={classes.title}>
                            HIV is just a normal disease
                        </Typography>
                        <Typography style={{ margin: "0 auto" }} variant="p" component="h1" className={classes.title}>
                            Access clinical tools and guidelines, continuing education, and patient materials on HIV screening, prevention, treatment, and care.
                        </Typography>

                        <Button variant="contained" style={{ background: "#212C2D", color: "#fff", padding: "15px", borderRadius: "12px" }} onClick={() => openModal()} className="">
                            <div style={{ display: "flex", alignItems: "center", gap: "10px" }}>
                                <PlayCircleOutlinedIcon />
                                <span>Play video</span>
                            </div>
                            {modal ? (
                                <section className="modal__bg">
                                    <div className="modal__align">
                                        <div className="modal__content" modal={modal}>
                                            <IoCloseOutline
                                                className="modal__close"
                                                arial-label="Close modal"
                                                onClick={setModal}
                                            />
                                            <div className="modal__video-align">
                                                {videoLoading ? (
                                                    <div className="modal__spinner">
                                                        <BiLoaderAlt
                                                            className="modal__spinner-style"
                                                            fadeIn="none"
                                                        />
                                                    </div>
                                                ) : null}
                                                <iframe
                                                    className="modal__video-style"
                                                    onLoad={spinner}
                                                    loading="lazy"
                                                    width="800"
                                                    height="500"
                                                    src="https://www.youtube.com/embed/9R7Qj24mvOE"
                                                    title="YouTube video player"
                                                    frameBorder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen
                                                ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            ) : null}
                        </Button>
                    </Box>
                </div>
            </section>
            <HIV />
            <Article />
        </React.Fragment>

    );
};

export default Main;