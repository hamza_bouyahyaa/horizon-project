import React from "react";
import { BrowserRouter } from "react-router-dom";
import routes, { renderRoutes } from "./routes";
import { AuthProvider } from "./contexts/JWTAuthContext";
import { ModalsProvider, Notifier } from "./components";
import CssBaseline from "@material-ui/core/CssBaseline";
import { SnackbarProvider } from "notistack";
import { SkeletonTheme } from "react-loading-skeleton";
const App = () => {
  return (
    <>
      <CssBaseline />
      <SnackbarProvider
        autoHideDuration={3000}
        maxSnack={12}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
      >
        <Notifier />
        <BrowserRouter>
          <AuthProvider>
            <SkeletonTheme color="#ebebeb" highlightColor="#000">
              {renderRoutes(routes)}
            </SkeletonTheme>
            <ModalsProvider />
          </AuthProvider>
        </BrowserRouter>
      </SnackbarProvider>
    </>
  );
};

export default App;
