import React, { useEffect, useState } from 'react';
import "./index.scss";
// import Popup from './Popup';
import axios from '../../utilities/axios'
import { TextField } from '@mui/material';


function Article() {

    const [images, setImages] = useState([]);
    const [image, setImage] = useState({});
    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState('')


    useEffect(async () => {
        setLoading(true);
        const response = await axios.get('https://api.u-equals-u.me/api/articles');
        const data = await response.data.data;
        setImages(data);
        setLoading(false);


    }, [])



    return (
        <>
            {loading ? <h4>Loading...</h4> :
                <ul class="cards">
                    {/* <Popup image={image} /> */}

                    {images?.map((image) => {
                        return (
                            <li key={image._id}>
                                <a class="button" href="#popup" className="card" onClick={async () => await setImage(image)}>
                                    <img src={`https://api.u-equals-u.me/img/${image?.image}`} className="card__image" alt="" />
                                    <div className="card__overlay">
                                        <div className="card__header">
                                            <svg className="card__arc" xmlns="http://www.w3.org/2000/svg"><path /></svg>
                                            <img className="card__thumb" src={image?.user?.profile_image?.small} alt="" />
                                            <div className="card__header-text">
                                                <h3 className="card__title">{image?.user?.firstName} {image?.user?.lastName}</h3>
                                            </div>
                                        </div>
                                        <p className="card__description">{image?.content || image?.content || 'No description'}</p>
                                    </div>
                                </a>
                                <span className="card__status">
                                    {image?.views} Views
                                </span>
                            </li>
                        )
                    })}

                </ul>
            }

        </>
    )
}

export default Article