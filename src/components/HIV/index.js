import React, { useEffect } from "react";
import { Card, Grid, Link } from "@mui/material";
import DATA from "./../../utilities/constants"
import "./_index.scss"
import Aos from "aos";
import "aos/dist/aos.css";


const HIV = () => {
    useEffect(() => {
        Aos.init({ duration: 2000 });
    }, []);
    return (
        <Grid container style={{ padding: "30px" }} data-aos="fade-up">
            {DATA.HIVINFO.map((el, index) => (
                <Grid item xs={6} className="card-wrapper" key={index}>
                    <Link href={el?.href} style={{ textDecoration: 'none' }} target='_blank'>
                        <Card className="card-header">
                            <img src={el.image} className="image-card" />
                            <span className="title-card">{el.name}</span>
                        </Card>
                    </Link>

                </Grid>
            ))}
        </Grid>


    )

}
export default HIV