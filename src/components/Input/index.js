import React, { useState } from "react";
import { useFormContext, Controller } from "react-hook-form";
import { IconButton } from "@mui/material";
import Iconify from "./../Iconify";
import MuiPhoneNumber from "material-ui-phone-number";
const Input = ({
  label,
  placeholder,
  name,
  errors,
  id,
  className,
  register,
  phoneNumber,
}) => {
  const { control } = useFormContext();
  const [showPassword, setShowPassword] = useState(false);
  return (
    <div className="label-input-wrapper">
      <label>
        {label}
        {id && <sup style={{ color: "red" }}>*</sup>}
      </label>
      {phoneNumber ? (
        <Controller
          name={name}
          control={control}
          rules={{ required: true }}
          render={({ field, fieldState: { error } }) => {
            return (
              <>
                <MuiPhoneNumber
                  variant="outlined"
                  defaultCountry="tn"
                  fullWidth
                  name={name}
                  InputProps={{
                    style: {
                      borderRadius: "10px",
                      backgroundColor: "white",
                    },
                  }}
                  onChange={(e) => {
                    field.onChange(e);
                  }}
                  error={Object?.keys(errors)?.includes(name) ? true : false}
                />
                {name === "phone" && <span>{errors?.phone?.message}</span>}
              </>
            );
          }}
        />
      ) : (
        <div
          className={
            Object?.keys(errors)?.includes(name)
              ? `input-Container ${className} input-container-error`
              : `input-Container ${className}`
          }
        >
          <Controller
            name={name}
            control={control}
            rules={{ required: true }}
            render={({ field, fieldState: { error } }) => {
              return (
                <>
                  <input
                    type={
                      name === "email"
                        ? "email"
                        : !showPassword && name.includes("password")
                        ? "password"
                        : "text"
                    }
                    placeholder={placeholder}
                    onChange={(e) => {
                      field.onChange(e.target.value);
                    }}
                    {...register(name)}
                  />

                  {name.includes("password") && (
                    <IconButton
                      onClick={() => setShowPassword(!showPassword)}
                      style={{
                        backgroundColor: "transparent",
                      }}
                    >
                      <Iconify
                        icon={
                          showPassword ? "eva:eye-fill" : "eva:eye-off-fill"
                        }
                      />
                    </IconButton>
                  )}
                </>
              );
            }}
          />
        </div>
      )}
      {Object.keys(errors).map((fieldName, i) => {
        return (
          fieldName === name && (
            <span key={i}>{errors[fieldName]?.message} </span>
          )
        );
      })}
    </div>
  );
};

export default Input;
