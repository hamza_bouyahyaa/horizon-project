import React, { useState, useEffect } from "react";
import { OperatorProfileImage } from "../../assets";
import { useDispatch, useSelector } from "react-redux";
import { DeleteProfilePic, setNewAvatar } from "../../slices/user";
import { Avatar, Button, CircularProgress } from "@mui/material";
import {
  enqueueSnackbar as enqueueSnackbarAction,
  closeSnackbar as closeSnackbarAction,
} from "../../slices/notifier";
const ProfileImage = ({ className, id, btnClassName, userImage }) => {
  const { user, statusDeleteProfilePicture } = useSelector(
    (state) => state.user
  );
  const enqueueSnackbar = (...args) => dispatch(enqueueSnackbarAction(...args));
  const closeSnackbar = (...args) => dispatch(closeSnackbarAction(...args));
  const showSnackbar = (data) => {
    // NOTE:
    // if you want to be able to dispatch a `closeSnackbar` action later on,
    // you SHOULD pass your own `key` in the options. `key` can be any sequence
    // of number or characters, but it has to be unique for a given snackbar.
    enqueueSnackbar({
      message: data.message,
      options: {
        key: new Date().getTime() + Math.random(),
        variant: data.variant,
        action: (key) => (
          <Button style={{ color: "white" }} onClick={() => closeSnackbar(key)}>
            dismiss me
          </Button>
        ),
      },
    });
  };
  const [image, setImage] = useState(
    id === "update-image"
      ? user.avatar
      : id === "add-operator-drawer"
      ? userImage
      : OperatorProfileImage
  );
  useEffect(() => {
    setImage(
      id === "update-image"
        ? user.avatar
        : id === "add-operator-drawer"
        ? userImage
        : OperatorProfileImage
    );
  }, [user]);
  const dispatch = useDispatch();
  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage(URL.createObjectURL(event.target.files[0]));
      dispatch(setNewAvatar(event.target.files[0]));
    }
  };
  let inputRef;
  return (
    <div className={className ? `image-profile ${className}` : `image-profile`}>
      {/* <div className="upload-image-profile"> Profile Image </div> */}
      <div className="img-file-wrapper">
        <Avatar className="profile-avatar" src={image} alt="preview image" />
        <div className="text-btns-wrapper">
          <div>
            {id === "add-operator-drawer"
              ? "Upload User Profile Image"
              : "Edit your profile"}
          </div>
          {id === "add-operator-drawer" ? (
            <span className="file-size">
              (File size: max 1024 KB | Formats: .PNG, .JPG)
            </span>
          ) : null}
          <div className="input-delete-btn-wrapper">
            <button
              type="button"
              name="upload-btn"
              className={`upload-btn ${btnClassName}`}
              onClick={() => {
                inputRef.click();
              }}
            >
              Upload
            </button>
            <input
              type="file"
              onChange={onImageChange}
              className="filetype"
              accept={"image/*"}
              ref={(refParam) => (inputRef = refParam)}
            />
            {id === "add-operator-drawer" ? null : (
              <button
                className="delete-btn"
                type="button"
                disabled={
                  statusDeleteProfilePicture === "loading" ? true : false
                }
                onClick={() =>
                  image !== OperatorProfileImage &&
                  (() => {
                    setImage(OperatorProfileImage);
                    dispatch(setNewAvatar(null));
                    dispatch(DeleteProfilePic()).then((res) => {
                      if (res.meta.requestStatus === "fulfilled") {
                        showSnackbar({
                          variant: "success",
                          message: "Profile Picture Deleted Successfully",
                        });
                      } else {
                        showSnackbar({
                          variant: "error",
                          message: res?.error?.message,
                        });
                      }
                    });
                  })()
                }
              >
                {statusDeleteProfilePicture === "loading" ? (
                  <CircularProgress size={15} style={{ color: "#ed3863" }} />
                ) : (
                  "Delete"
                )}
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfileImage;
